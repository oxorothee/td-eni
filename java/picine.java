import java.util.Random;

public class nageurs {
    //public class var
    public static int[][] tabNageurPos = new int[3][2];
    public static String[] tabNageurName = new String[3];
    public static int podium;

    public static Random rd = new Random();

    public static void main(String[] args) {
        // init position
        tabNageurName[0] = "Bousquet";
        tabNageurName[1] = "Le Veau";
        tabNageurName[2] = "Dubosc";

        while (finishRun()){
            for(int nag=0;nag<3;nag++){
                if(tabNageurPos[nag][0] == 50) continue; // si le nageur a deja atteint la ligne d'arriver

                else{ // fait avancer le nageur et affiche la picine
                    avanceNageur(nag);
                    view_picine();
                }

            }
        }
        print_Podium();
    }

    private static void print_Podium() {
        String podium = "";
        int place = 1;
        while (true){
            for(int i = 0; i <3; i++){
                if(tabNageurPos[i][1] == place){
                    System.out.println(place + " " + tabNageurName[i]);
                    place++;
                }
            }
            if(place == 4)break;
        }
    }

    private static void avanceNageur(int nag) { // fait avancer le nagueur
        int pulsion = rd.nextInt(3);
        if(tabNageurPos[nag][0] + pulsion <50){
            tabNageurPos[nag][0] = tabNageurPos[nag][0] + pulsion;
        }
        else{
            tabNageurPos[nag][0] = 50;
            podium++;
            tabNageurPos[nag][1] = podium;
        }
    }

    private static boolean finishRun() { // verifie que la cours est terminer
        if(tabNageurPos[0][1] == 0 || tabNageurPos[1][1] == 0 || tabNageurPos[2][1] == 0){
            return true;
        }
        return false;
    }

    private static void view_picine(){
        // affiche l'a picine dans la console
        coter_bassin();
        for(int nag = 0; nag <3;nag++){ // pour chaque nagueur
            String ligne = nageurNameAndSpace(tabNageurName[nag]) + "||";
            for(int pos = 0; pos<25;pos++) {
                if(positionOfNagueur((pos),tabNageurPos[nag][0])) ligne = ligne + nagueurSkin(tabNageurPos[nag][0]);
                else ligne = ligne + "   ";
            }
            ligne = ligne + "||" + tabNageurPos[nag][0] + "/50";
            System.out.println(ligne);

        }
        coter_bassin();
    }

    private static String nageurNameAndSpace(String nageurName) {
        return switch (nageurName.length()){
            case 10 -> nageurName;
            case 9 -> nageurName + " ";
            case 8 -> nageurName + "  ";
            case 7 -> nageurName + "   ";
            case 6 -> nageurName + "    ";
            case 5 -> nageurName + "     ";
            case 4 -> nageurName + "      ";
            default -> nageurName;
        };
    }

    private static String nagueurSkin(int i) {
        if(i > 25){
            return "O#=";
        }
        return "=#O";
    }

    private static boolean positionOfNagueur(int posPicine, int posNageur) {
        if(posNageur <= 25){ //aller
            if(posPicine == posNageur) return true;
        }
        else{ // retour
            if((50 - posNageur) == posPicine) return true;
        }
        return false;
    }

    private static void coter_bassin(){
        for(int i =0; i < 37; i++){ // coter du bassin
            if(i>10)System.out.print("===");
            else System.out.print(" ");

        }
        System.out.print("\n");
    }
}
