import java.util.Scanner;

public class MyMainClass {
    public static Scanner input = new Scanner(System.in);
    public static int[][] plateau = new int[3][3];
    public static boolean tourjeu = true;

    public static void main(String[] args) {
	// write your code here
        int etat;


        for (int round = 0; round < 9; round++){ // 9 tours de jeu

            view_plateau();
            print_a_qui_de_jouer();
            saisie_case();

            etat = etat_partie();
            if (etat != 0){
                System.out.println("le joueur"+ etat +" gagne la partie");
                break;
            }
            else if (round == 8){
                System.out.println("match null");
            }


        }
    }
    private static int rowBySaisie( int nomber) { // retourn la ligne en fonction de ca position sur le paver numerique
        return switch (nomber){
            case 1 -> 2;
            case 2 -> 2;
            case 3 -> 2;
            case 4 -> 1;
            case 5 -> 1;
            case 6 -> 1;
            case 7 -> 0;
            case 8 -> 0;
            case 9 -> 0;
            default -> 1;
        };
    }
    private static int colBySaisie(int nomber) { // retourn la colone en fonction de ca position sur le paver numerique
        return switch (nomber){
            case 1 -> 0;
            case 2 -> 1;
            case 3 -> 2;
            case 4 -> 0;
            case 5 -> 1;
            case 6 -> 2;
            case 7 -> 0;
            case 8 -> 1;
            case 9 -> 2;
            default -> 1;
        };
    }
    private static void saisie_case() {
        /*
        recupere la case saisie part l'utilisateur sur le paver numerique
        controle qu'elle est valide
        controle que la case est libre sur le plateau
        place le 'pion' du joueur au bonne endroit sur le plateau
         */
        int saisie,col,row;
        System.out.println("entrez un nombre de 1 a 9");
        while (true){
            saisie = input.nextInt();
            if(saisie > 0 && saisie <10){
                col = colBySaisie(saisie);
                row = rowBySaisie(saisie);
                if(plateau[row][col] == 0){
                    if(tourjeu) plateau[row][col] = 1;
                    else plateau[row][col] = 2;
                    change_joueur();
                    break;
                }
                else System.out.println("case deja prise");

            }
            else System.out.println("ce n'est pas une case du plateau");
        }
    }
    private static void change_joueur() {
        if (tourjeu) tourjeu = false;
        else tourjeu = true;
    }
    private static int etat_partie() { /* control qu'un joueur est gagner ou non
        \ return 0: aucun joueur n'a gagner
        \ return 1: le joueur 1 a gagner
        \ return 2: le joueur 2 a gagner
        */
        for(int i =0; i <3;i++){
            if(plateau[i][0] == plateau[i][1] && plateau[i][0] == plateau[i][2] && plateau[i][0] != 0){
                return plateau[i][0];
            }
            else if(plateau[0][i] == plateau[1][i] && plateau[0][i] == plateau[2][i] && plateau[0][i] != 0) {
                return plateau[0][i];
            }
        }
        if(plateau[0][0] == plateau[1][1] && plateau[0][0] == plateau[2][2] && plateau[0][0] != 0){
            return plateau[0][0];
        }
        else if(plateau[2][0] == plateau[1][1] && plateau[0][0] == plateau[0][2] && plateau[2][0] != 0){
            return plateau[0][0];
        }

        return 0;
    }
    private static void view_plateau(){ // affiche le plateau du jeu
        for(var i = 0; i <3; i++){
            var ligne = "";
            for(var a = 0; a <3; a++){
                switch (plateau[i][a]) {
                    case 0 -> ligne = ligne + ".";
                    case 1 -> ligne = ligne + "X";
                    case 2 -> ligne = ligne + "O";
                }
                if(a != 2){
                    ligne = ligne + "|";
                }

            }
            System.out.println(ligne);
        }
    }
    private static void print_a_qui_de_jouer(){ // indique qu'elle joueur doit jouer
        if(tourjeu){
            System.out.println("c'est au tour du joueur 1");
        }
        else {
            System.out.println("c'est au tour du joueur 2");
        }
    }
}
